import time
import logging
from weakref import WeakKeyDictionary

from twisted.internet import reactor
import scrapy
from scrapy.exceptions import IgnoreRequest
from scrapy import signals
from scrapy.exceptions import DontCloseSpider

from .vndb_api_handler import VNDBSocket, VNDBThrottled

class VNDBAPIResponse(scrapy.http.Response):
    def __init__(self, callback, _id = 1, dtype = "vn", content={}):
        super().__init__(url="http://example.com")
        self._id = _id
        self.dtype = dtype
        self.content = content
        
class VNDBAPIDownloader():

    requests = WeakKeyDictionary()

    def __init__(self, vndb_user="", vndb_pass=""):
        self.vndb = VNDBSocket("api.vndb.org", 19534, vndb_user, vndb_pass)
        self.log = logging.getLogger('scrapy')
        self.log.setLevel(logging.WARNING)

    def process_request(self, request, spider):
        try:
            result = self.vndb.get("id >= %s" % request._id, dtype=request.dtype)
        except VNDBThrottled:
            self.log.critical("Throttled")
            self.requests.setdefault(spider, 0)
            self.requests[spider] += 1
            reactor.callLater(60, self.schedule_request, request, spider)
            raise IgnoreRequest()

        self.log.critical(request._id)
        return VNDBAPIResponse(spider.parse, _id = request._id, dtype=request.dtype, content = result)

    def schedule_request(self, request, spider):
        self.requests[spider] -= 1
        spider.crawler.engine.schedule(request, spider)

    @classmethod
    def from_crawler(cls, crawler):
        ext = cls(
            vndb_user=crawler.settings.get('VNDB_USER'),
            vndb_pass=crawler.settings.get('VNDB_PASS')
        )
        crawler.signals.connect(ext.spider_idle, signal=signals.spider_idle)
        return ext
    @classmethod
    def spider_idle(cls, spider):
        if cls.requests.get(spider):
            spider.log.critical("delayed requests pending, not closing spider")
            raise DontCloseSpider()