import sys
from typing import List

import scrapy

from .base_spider import BaseSpider

class MALAnimeSpider(BaseSpider):
    """Scraper for anime pages on MyAnimeList."""
    name = "MAL_anime"
    # xpath for getting things in the left column
    _lc_xpath: str = "//div[@class='js-scrollfix-bottom']/div[contains(.//span, '{}:')]/descendant-or-self::*/text()"
    

    def get_lc(self, response, text) -> List[str]:
        """Gets data from the column on the left of the page.

        Args:
            response (`scrapy.http.HtmlResponse`_): The scrapy response.
            text (str): The text to search for.
        
        Returns:
            List[str]: results
        
        
        Example:
            ::

                >>> get_lc(response, "Studios")
                ['Gainax', 'Production I.G']

        Todo:
            * We might want to retain information such as hyperlinks; that is discarded right now.
        
        .. _scrapy.http.HtmlResponse:
           https://doc.scrapy.org/en/latest/topics/request-response.html#scrapy.http.HtmlResponse
        """
        raw: List(str) = response.xpath(self._lc_xpath.format(text)).extract()
        results = []
        for r in raw:
            r = r.replace('  ',' ').replace('\n','').strip()
            if r and r != ' ' and r != text + ':' and r != ',':
                results.append(r)
        return results

    def start_requests(self):
        for anime_id in range(self.start, self.end):
            yield scrapy.Request(url=self.settings["MAL_ANIME_PATH"] % str(anime_id), callback=self.parse)

    def parse(self, response):
        # URL: of the format "file:///anime/ID.html"
        mal_id = int(response.url.split('/')[-1].split(".")[0])
        is404 = response.xpath('//*[@id="contentWrapper"]/div[1]/h1/text()').extract_first()
        is404option2 = response.xpath('/html/head/title/text()').extract_first()
        if is404 == "404 Not Found" or is404option2 == "404 Not Found":
            print("%s %s"%(str(mal_id),(is404 or is404option2)))
            yield None
            return

        print("%s %s"%(str(mal_id), "Found."))
        title = response.xpath('//span[@itemprop="name"]/text()').extract_first()

        # General left column info; some of these still need processing later!
        try:
            episodes = int(self.get_lc(response, 'Episodes')[0])
        except:
            episodes = None

        type_:str = self.get_lc(response, 'Type')[0]
        status: str = self.get_lc(response, 'Status')[-1] # All but the last come from the "edit status" box
        aired: str = self.get_lc(response, 'Aired')[0]
        source: str = self.get_lc(response, 'Source')[0]
        duration: str = self.get_lc(response, 'Duration')[0]
        rating: str = self.get_lc(response, 'Rating')[0]

        producers: List[str] = self.get_lc(response, 'Producers')
        licensors: List[str] = self.get_lc(response, 'Licensors')
        studios: List[str] = self.get_lc(response, 'Studios')
        genres: List[str] = self.get_lc(response, 'Genres')

        description = ''.join(response.xpath(
            "//*[@id='content']/table/tr/td[2]/div[1]/table/tr[1]/td/span"
        ).extract())

        raw_rels = response.xpath(
            "//table[@class='anime_detail_related_anime']//tr"
        ).extract()

        rels = {}

        for rel in raw_rels:
            out = []
            rel_name = rel.split('borderClass">')[1].split(':')[0]
            rel_links = [x.replace(' ', '') for x in rel.split('borderClass">')[2].split('>,')]
            for link in rel_links:
                split_link = link.split('/')
                # Links might be borked and empty
                try:
                    out.append((split_link[1], int(split_link[2])))
                except:
                    pass

            rels[rel_name] = out

        ops = response.xpath("//div[@class='theme-songs js-theme-songs opnening']/span/text()").extract()
        eds = response.xpath("//div[@class='theme-songs js-theme-songs ending']/span/text()").extract()

        main_img = response.xpath(
            "//*[@id='content']/table/tr/td[1]/div/div[1]/a/img/@src"
        ).extract_first()

        synonyms = []
        for x in response.xpath("//div[@class='spaceit_pad']").extract()[:3]:
            try:
                synonyms.append(x.split('">')[2].replace('\n  </div>','').replace('</span> ',''))
            except:
                pass

        raw_score = response.xpath("//div[@class='fl-l score']/text()").extract_first()
        try:
            score = float(raw_score)
        except ValueError:
            assert(raw_score == "N/A", "Real value: "+str(raw_score))
            score = None
        try:
            raw_num_scorers = response.xpath("//span[@itemprop='ratingCount']/text()").extract_first().replace(',', '')
        except AttributeError:
            raw_num_scorers = response.xpath("//div[@class='po-r js-statistics-info di-ib']/span[3]/text()").extract_first().replace(',', '')
        try:
            num_scorers = int(raw_num_scorers)
        except ValueError:
            assert(raw_num_scorers == "N/A", "Real value: "+str(raw_num_scorers))
            num_scorers = None

        raw_ranked = response.xpath("//span[@class='numbers ranked']/strong/text()").extract_first().replace('#', '')
        try:
            ranked = int(raw_ranked)
            if ranked == 0:
                ranked = None
        except ValueError:
            assert(raw_ranked == "N/A", "Real value: "+str(raw_ranked))
            ranked = None

        raw_popularity = response.xpath("//span[@class='numbers popularity']/strong/text()").extract_first().replace('#', '')
        try:
            popularity = int(raw_popularity)
            if popularity == 0:
                popularity = None
        except ValueError:
            assert(raw_popularity == "N/A", "Real value: "+str(raw_popularity))
            popularity = None

        raw_members = response.xpath("//span[@class='numbers members']/strong/text()").extract_first().replace(',', '')
        try:
            members = int(raw_members)
        except ValueError:
            assert(raw_members == "N/A", "Real value: "+str(raw_members))
            members = None

        yield {
            'score': score,
            'num_scorers': num_scorers,
            'ranked': ranked,
            'popularity': popularity,
            'members': members,
            'mal_id': mal_id,
            'title': title,
            'num_episodes': episodes,
            'status': status,
            'type': type_,
            'aired': aired,
            'source': source,
            'duration': duration,
            'rating': rating,
            'producers': producers,
            'licensors': licensors,
            'studios': studios,
            'genres': genres,
            'description': description,
            'relations': rels,
            'OPs': ops,
            'EDs': eds,
            'main_img': main_img,
            'synonyms': synonyms
        }
