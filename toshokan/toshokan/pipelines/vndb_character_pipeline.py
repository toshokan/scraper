from datetime import datetime
import re
import json
import scrapy
from pymongo import InsertOne, UpdateOne
from bson.dbref import DBRef

from .mal_pipeline import LibraryPipeline

class VNDBCharacterPipeline(LibraryPipeline):
    coll = "vn_character"
    trait_coll = "vn_trait"
    def open_spider(self, *args, **kwargs):
        super().open_spider(*args, **kwargs)
        self.load_traits()
        #self.add_traits()
        
    def load_traits(self):
        with open("/var/lib/scrapyd/files/traits.json", "r", encoding="utf-8") as f:
            self.traitsdb = json.load(f)
        self.traitsdict = {}
        for trait in self.traitsdb:
            self.traitsdict[trait["id"]] = trait

    def add_traits(self):
        ops = []
        for trait in self.traitsdb:
            path = self.get_path(trait["id"])
            ops.append(UpdateOne(
            {
                "vndb": trait["id"]
            },
            {
                "$set": {
                    "current_name": trait["name"],
                    "aliases": trait["aliases"],
                    "description": trait["description"],
                    "meta": trait["meta"],
                    "char_count": trait["chars"],
                    "parents": list(self.get_parent_traits(trait["id"])),
                    "last_modified": datetime.utcnow()
                },
                "$setOnInsert": {
                    "_id": self.get_pathid(trait["id"]),
                    "vndb": trait["id"],
                    "path": path,
                    "type": path[0],
                    "name": trait["name"],
                    "date_added": datetime.utcnow()
                }
            }, upsert=True))
        self.db[self.trait_coll].bulk_write(ops)

    def get_parent_traits(self, id):
        for parent in self.traitsdict[id]["parents"]:
            entry = self.db[self.trait_coll].find_one({"vndb": self.traitsdict[parent]["id"]})
            if entry:
                yield entry["_id"]
            else:
                yield self.get_pathid(parent)

    def get_pathid(self, id):
        return ".".join([self.library_id(name) for name in self.get_path(id)])

    def get_path(self, id, path=[]):
        parents = self.traitsdict[id]["parents"]
        path = [self.traitsdict[id]["name"]] + path
        if len(parents) > 0:
            return self.get_path(min(parents), path)
        return path

    def process_item(self, item, spider):
        if spider.name != "VNDB_API_Character":
            return item
        self.log.warning(item["id"])
        traits, tdata = self.process_traits(item["traits"])
        instances = list(self.process_instances(item["instances"]))
        char = {
            "source": "vndb",
            "nick_name": item["name"],
            "name_english": item["name"],
            "name_japanese": item["original"],
            "aliases": item["aliases"],
            "image": item["image"],
            "gender": item["gender"],
            "blood": item["bloodt"],
            "birthday": item["birthday"],
            "description": item["description"],
            "measure": {
                "bust": item["bust"],
                "waist": item["waist"],
                "hip": item["hip"],
                "height": item["height"],
                "weight": item["weight"]
            },
            "traits": traits,
            "trait_spoiler": tdata,
            "external_links": {"vndb": item["id"]},
            "last_modified": datetime.utcnow(),
        }
        prev = self.db[self.coll].find_one({"external_links.vndb": item["id"]})
        if not prev:
            char["_id"] = self.create_id(self.coll, item["id"], item["name"])
            char["new"] = True
            self.log.warning('Adding new Character: %s %s', str(item["id"]), char["_id"])

            if char["_id"] != self.library_id(char["name_english"]):
                char["dup_id"] = True
                self.log.error('New character (%s) computed ID %s was a duplicate: %s was used instead.', str(item["id"]), self.library_id(char["name_english"]), char["_id"])
        else:
            char["_id"] = prev["_id"]

        self.db[self.coll].update_one({"external_links.vndb":item["id"]}, {"$set":char, "$setOnInsert": {"date_added": datetime.utcnow()}}, upsert=True)
        self.process_vns(item["vns"], char["_id"], char["name_english"])
        return item

    def process_traits(self, trait_array):
        traits = []
        trait_spoiler = []
        for trait in trait_array:
            try:
                trait_info = self.db[self.trait_coll].find_one({"vndb": trait[0]})
                if not trait_info:
                    continue
                traits.append([trait_info["type"], trait_info["name"]])
                trait_spoiler.append(trait[1])
            except:
                continue
        return traits, trait_spoiler
    def process_instances(self, instances):
        for inst in instances:
            instance = {
                "nick_name": inst["name"],
                "name_english": inst["name"],
                "name_japanese": inst["original"],
                "spoiler": inst["spoiler"]
            }
            character = self.db[self.coll].find_one({"external_links.vndb": inst["id"]})
            if character:
                instance["_id"] = character["_id"]
                yield instance
            else:
                # create character if it doesn't already exist
                char = {
                    "_id": self.create_id(self.coll, inst["id"], inst["name"]),
                    "source": "vndb",
                    "nick_name": inst["name"],
                    "name_english": inst["name"],
                    "name_japanese": inst["original"],
                    "external_links": {"vndb": inst["id"]},
                    "last_modified": datetime.utcnow(),
                    "date_added": datetime.utcnow()
                }
                instance["_id"] = char["_id"]
                self.log.warning('Adding new Character (From reference): %s %s', str(inst["id"]), char["_id"])
                self.db[self.coll].insert_one(char)
                yield instance

    def process_vns(self, vns, _id, name):
        ops = []
        for vn in vns:
            vn_data = self.db.vn.find_one({"external_links.vndb": vn[0]})
            if not vn_data:
                continue
            vn_ref = DBRef("vn", vn_data["_id"], title=vn_data["title"])
            char_ref = DBRef("character", _id, name=name)
            ops.append(UpdateOne(
            {
                "vn.$id": vn_data["_id"],
                "character.$id": _id,
            },
            {"$set": {
                "vn": vn_ref,
                "character": char_ref,
                "release": vn[1],
                "spoiler": vn[2],
                "type": vn[3].capitalize(),
                "last_modified": datetime.utcnow(),
            }, "$setOnInsert": {"date_added": datetime.utcnow(), "voices": []}}, upsert=True))
        if ops:
            self.db.vn_cast.bulk_write(ops)