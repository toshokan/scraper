import scrapy, re, logging

class AniDBPersonSpider(scrapy.Spider):
    name = "anidb_person"

    def start_requests(self):
        pass

    def parse(self, response):
        out = {}
        # TODO uid
        uid = 1776148842069133735731151187

        out['main_name'] = response.xpath("//td/span[@itemprop='name']").extract_first()
        out['alt_names'] = {}
        for row in response.xpath("//div[@id='tab_1_2_pane']//tr").extract()[1:]:
            if 'language' in row:
                out['alt_names'][row.split('language: ')[1].split('"')[0]] = row.split('Name">')[1].split('</label')[0]
            else:
                out['alt_names'][row.split('field">')[1].split('</')[0]] = row.split('value">')[1].split('</td>')[0]

        
        
