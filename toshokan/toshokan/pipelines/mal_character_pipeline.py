from datetime import datetime
import re

import scrapy
from pymongo import InsertOne, UpdateOne
from bson.dbref import DBRef

from .mal_pipeline import MALPipeline

class MALCharacterPipeline(MALPipeline):
    coll = 'character'

    def process_item(self, item, spider):

        if spider.name != "MAL_character":
            return item

        if item == None:
            raise scrapy.exceptions.DropItem

        character = {
            'nick_name':item["name_with_nickname"],
            'name_english':item["name_english"],
            'name_japanese':item["name_japanese"],
            'image':item["main_img"],
            'description':item["description"],
            'external_links':{"mal": item["mal_id"]},
            'last_modified': datetime.utcnow()
        }
        prev = self.db.character.find_one({"external_links.mal":item["mal_id"]}, {"_id": 1})
        if not prev:
            character["_id"] = self.create_id(self.coll, item["mal_id"], item["name_english"])
            character["date_added"] = datetime.utcnow()
            self.log.warning('Adding new character: %s %s', str(item["mal_id"]), character["_id"])

            if character["_id"] != self.library_id(item["name_english"]):
                character["dup_id"] = True
                self.log.error('New character (%s) computed ID %s was a duplicate: %s was used instead.', str(item["mal_id"]), self.library_id(item["name_english"]), character["_id"])
        else:
            character["_id"] = prev["_id"]
        self.db[self.coll].update_one({"external_links.mal":item["mal_id"]}, {"$set": character}, upsert=True)
        return item