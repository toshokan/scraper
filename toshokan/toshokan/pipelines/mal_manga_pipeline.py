from datetime import datetime
import re

import scrapy
from pymongo import InsertOne, UpdateOne
from bson.dbref import DBRef

from .mal_pipeline import MALPipeline

class MALMangaPipeline(MALPipeline):
    coll = '%s'

    def process_item(self, item, spider):
        if spider.name != "MAL_manga":
            return item

        if item == None:
            raise scrapy.exceptions.DropItem
        dates = self.air_time(item["published"])
        manga = {
            "title": item["title"],
            "synonyms": self.parse_synonyms(item["synonyms"]),
            "image": item["main_img"],
            "type": item["type"],
            "status": item["status"],
            "chapter_count": item["num_chapters"],
            "volume_count": item["num_volumes"],
            "start_date": dates[0]['date_obj'],
            "end_date": dates[1]['date_obj'],
            "start_date_period": dates[0]['period'],
            "end_date_period": dates[1]['period'],
            "authors": [self.get_author(author) for author in item["authors"]],
            "serialization": self.get_serialization(item["serialization"]),
            "genres": [p for p in item["genres"] if p not in ["No genres have been added yet."]],
            "description": item["description"],
            "links": self.resolve_relations(item["relations"]),
            "external_links":{"mal": item["mal_id"]},
            "last_modified": datetime.utcnow()
        }

        typedb = "novel" if item["type"] == "Novel" else "manga"

        prev = self.db[typedb].find_one({"external_links.mal": item["mal_id"]})

        if not prev:
            manga["_id"] = self.create_id(typedb, item["mal_id"], item["title"], manga["synonyms"])
            manga["date_added"] = datetime.utcnow()
            manga["new"] = True
            self.log.warning('Adding new manga: %s %s', str(item["mal_id"]), manga["_id"])
            dup_title = self.db[typedb].find_one({"title": manga["title"]})
            if dup_title:
                manga["dup_title"] = True
                self.log.error('New manga (%s) title (%s) was a duplicate: %s was set as ID.', str(item["mal_id"]), item["title"], manga["_id"])
        else:
            manga["_id"] = prev["_id"]

        upd = self.db[self.coll % typedb].update_one({"external_links.mal":item["mal_id"]}, {"$set":manga}, upsert=True)
        return item

    def get_serialization(self, ser):
        if len(ser) == 0:
            return None
        if ser[0] == "None":
            return None
        else:
            return ser[0]

    def parse_synonyms(self, synonyms):
        syns = {}
        for syn in synonyms:
            if syn[0:8] == "English:":
                syns["english"] = syn[8:-6]
            elif syn[0:9] == "Japanese:":
                syns["japanese"] = syn[9:-6]
            elif syn[0:9] == "Synonyms:":
                syns["synonyms"] = syn[9:-6]
        return syns

    def get_author(self, aut):
        authobj = self.db.person.find_one({"external_links.mal":aut["id"]}, {"_id":1})
        return DBRef("person", str(authobj["_id"]), name=aut["name"], roles=aut["roles"])

    def resolve_relations(self, rel):
        relations = {}
        if rel.get("Adaptation"):
            relations["Adaptation"] = []
            for mrel in rel["Adaptation"]:
                anime = self.db.anime.find_one({"external_links.mal":mrel[1]}, {"title":1, "_id":1})
                
                if not anime:
                    relations["Adaptation"].append({"missing_ref":"anime", "missing_id":mrel[1]})
                    self.log.critical("Missing anime (Adaptation): %s", str(mrel[1]))
                    continue
                relations["Adaptation"].append(DBRef("anime", anime["_id"], title = anime["title"]))
            del rel["Adaptation"]

        for (t, rels) in rel.items():
            relations[t] = []
            for arel in rel[t]:
                manga = self.db.manga.find_one({"external_links.mal":arel[1]}, {"title":1, "_id":1, "type":1})
                if not manga:
                    manga = self.db.novel.find_one({"external_links.mal":arel[1]}, {"title":1, "_id":1, "type":1})
                if not manga:
                    relations[t].append({"missing_ref":"manga", "missing_id":arel[1]})
                    self.log.critical("Missing manga ("+t+"): %s", str(arel[1]))
                    continue
                ref = "novel" if manga["type"] == "Novel" else "manga"
                relations[t].append(DBRef(ref, manga["_id"], title = manga["title"]))
        return relations