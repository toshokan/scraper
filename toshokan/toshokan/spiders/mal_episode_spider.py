import scrapy

from .base_spider import BaseSpider

class MALEpisodeSpider(BaseSpider):
    """Scraper for episode pages on MyAnimeList."""
    name = "MAL_episodes"
    custom_settings = {
        #'SOME_SETTING': 'some value',
    }
    def start_requests(self):
        super().start_requests()
        anime_list = sorted([a["external_links"]["mal"] for a in list(self.db.anime.find({}, {"external_links.mal":1}))])
        self.log.critical(anime_list[0:10])
        self.log.critical(len(anime_list))
        if self.start and (self.end > 1):
            anime_list = anime_list[self.start-1: self.end]
            self.log.critical(len(anime_list))
        for anime_id in anime_list:
            yield scrapy.Request(url="https://myanimelist.net/anime/" + str(anime_id) + "/a/episode",
                                 callback=self.parse)

    def parse(self, response):
        mal_id = response.url.split('/')[-3]
        episode_numbers = set(response.xpath("//*[@class='episode-list-data']/td[1]/text()").extract())
        for episode in episode_numbers:
            yield scrapy.Request(url="https://myanimelist.net/anime/" + mal_id + '/a/episode/' + episode,
                                 callback=self.parse_episode)

    def parse_episode(self, response):
        mal_id = int(response.url.split('/')[-4])
        episode_num = int(response.url.split('/')[-1])

        try:
            title_en = response.xpath("//h2[@class='fs18 lh11']/text()").extract()[1]
        except:
            title_en = ''

        try:
            title_jp = response.xpath("//p[@class='fn-grey2']/text()").extract_first().strip()
        except:
            title_jp = ''

        tags = [x.strip() for x in response.xpath(
            "//span[@clas='ml8 icon-episode-type-bg']/text()"
        ).extract()]

        description = ''.join([(x.strip() if x != '\r\n' else x) for x in response.xpath(
            "//*[@id='content']/table/tr/td[2]/div/div[2]/div[1]/table/tr[1]/td/div[4]/text()"
        ).extract()])

        characters = [int(x.split('/')[-3]) for x in response.xpath(
            "//a[@class='fw-b']"
        ).extract()]

        people = [int(x.split('/')[-2]) for x in response.xpath(
            "//a[@class='fw-b ww-bw pr12']/@href"
        ).extract()]

        yield {
            'mal_id':mal_id,
            'episode_num':episode_num,
            'title_en':title_en,
            'title_jp':title_jp,
            'tags':tags,
            'description':description,
            'characters':characters,
            'people':people
        }
