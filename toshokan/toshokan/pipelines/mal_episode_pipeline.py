from datetime import datetime
import re

import scrapy
from pymongo import InsertOne, UpdateOne
from bson.dbref import DBRef

from .mal_pipeline import MALPipeline

class MALEpisodePipeline(MALPipeline):
    coll = 'anime_episode'

    def process_item(self, item, spider):

        if spider.name != "MAL_episodes":
            return item

        if item == None:
            raise scrapy.exceptions.DropItem
        
        anime = self.db.anime.find_one({"external_links.mal": item["mal_id"]}, {"_id": 1, "title":1})
        anime_ref = DBRef("anime", anime["_id"], title=anime["title"])
        episode = {
            'anime': anime_ref,
            'number': item["episode_num"],
            'title_english': item["title_en"],
            'title_japanese': item["title_jp"],
            'tags': item["tags"],
            'description': item["description"],
            'characters': [self.get_character(char) for char in item["characters"]],
            'people': [self.get_person(person) for person in item["people"]]
        }
        self.db[self.coll].update_one({"anime":anime_ref, "number": episode["number"]}, {"$set": episode}, upsert=True)
        return item
    
    def get_character(self, character_id):
        char = self.db["character"].find_one({"external_links.mal":character_id}, {"_id":1, "nick_name":1})
        if not char:
            self.log.critical("Character %d not found!" % character_id)
            raise ValueError
        return DBRef("character", char["_id"], name=char["nick_name"])

    def get_person(self, person_id):
        person = self.db["person"].find_one({"external_links.mal": person_id}, {"_id":1, "name":1})
        if not person:
            self.log.critical("Person %d not found!" % person_id)
            raise ValueError
        return DBRef("person", person["_id"], name=person["name"])