import socket
import json
import time

class VNDBError(Exception):
    pass
class VNDBThrottled(VNDBError):
    pass
class VNDBSocket:
    """VNDB TCP Protocol Handler
    Logins and sends messages, receives responses.
    Use command(name = "command", data = {key:value}) to send commands to VNDB.
    """
    EOL = b'\x04'
    def __init__(self, host, port, username, password):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.connect(host, port)
        self.login(username, password)

    def connect(self, host, port):
        self.sock.connect((host, port))

    def login(self, username, password):
        return self.command("login", { "protocol":1, "client":"13-50", "clientver":"8.8.41", "username": username, "password": password }, False)

    def get(self, filters, options={ "results": 25 }, dtype="vn"):
        if dtype == "vn":
            selectors = "basic,details,anime,relations,tags,stats,screens"
        elif dtype == "character":
            selectors = "basic,details,meas,traits,vns,instances"
        elif dtype == "staff":
            selectors = "basic,details,aliases,vns,voiced"
        elif dtype == "producer":
            selectors = "basic,details,relations"
        elif dtype == "release":
            selectors = "basic,details,vn,producers"
        return self.command("get", "%s %s (%s) %s" % (dtype, selectors, filters, json.dumps(options)), True, False)

    def command(self, name, data=None, res_json=True, data_json=True):
        if data and data_json:
            cmd = name + " " + json.dumps(data)
        elif data:
            cmd = name + " " + data
        else:
            cmd = name
        self.send(self.msg(cmd))
        response = self.recv().decode("utf-8")[0:-1]

        if response.startswith("error"):
            try:
                msg = json.loads(response[len("error")+1:])
            except:
                raise VNDBError(response)
            if msg["id"] == 'throttled':
                    raise VNDBThrottled
            raise VNDBError(msg["id"])
        if res_json:
            if name == "get":
                json_text = response[len("results")+1:]
            else:
                json_text = response[len(name)+1:]
            return json.loads(json_text)
        return response

    def msg(self, text):
        return text.encode("utf-8") + self.EOL

    def send(self, msg):
        msg_len = len(msg)
        totalsent = 0
        while totalsent < msg_len:
            sent = self.sock.send(msg[totalsent:])
            if sent == 0:
                raise RuntimeError("socket connection broken")
            totalsent = totalsent + sent

    def recv(self):
        chunks = []
        bytes_recd = 0
        while len(chunks) == 0 or not chunks[-1].endswith(self.EOL):
            chunk = self.sock.recv(2048)
            if chunk == b'':
                raise RuntimeError("socket connection broken")
            chunks.append(chunk)
            bytes_recd = bytes_recd + len(chunk)
        return b''.join(chunks)