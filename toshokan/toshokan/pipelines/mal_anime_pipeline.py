from datetime import datetime
import re

import scrapy
from pymongo import InsertOne, UpdateOne
from bson.dbref import DBRef

from .mal_pipeline import MALPipeline

class MALAnimePipeline(MALPipeline):
    coll = 'anime'

    def process_item(self, item, spider):
        if spider.name != "MAL_anime":
            return item

        if item == None:
            raise scrapy.exceptions.DropItem
        dates = self.air_time(item["aired"])
        anime = {
                    "title": item["title"],
                    "synonyms": self.parse_synonyms(item["synonyms"]),
                    "image": item["main_img"],
                    "type": item["type"],
                    "status": item["status"],
                    "episode_count": item["num_episodes"],
                    "duration": self.duration(item["duration"]),
                    "start_date": dates[0]['date_obj'],
                    "end_date": dates[1]['date_obj'],
                    "start_date_period": dates[0]['period'],
                    "end_date_period": dates[1]['period'],
                    "source": item["source"],
                    "genres": [p for p in item["genres"] if p not in ["No genres have been added yet."]],
                    "rating": item["rating"],
                    "producers": [p for p in item["producers"] if p not in ["None found,", "add some"]],
                    "licensors": [p for p in item["licensors"] if p not in ["None found,", "add some"]],
                    "studios": [p for p in item["studios"] if p not in ["None found,", "add some"]],
                    "description": item["description"],
                    "op": [self.remove_number(op) for op in item["OPs"]],
                    "ed": [self.remove_number(ed) for ed in item["EDs"]],
                    "links": self.resolve_relations(item["relations"]),
                    "external_links":{"mal": item["mal_id"]},

                    'score': item["score"],
                    'score_rank': item["ranked"],
                    'n_scores': item["num_scorers"],
                    'popularity_rank': item["popularity"],
                    'n_members': item["members"],

                    "last_modified": datetime.utcnow(),
        }
        prev = self.db.anime.find_one({"external_links.mal":item["mal_id"]})
        if not prev:
            anime["_id"] = self.create_id("anime", item["mal_id"], item["title"], anime["synonyms"])
            anime["date_added"] = datetime.utcnow()
            anime["new"] = True
            self.log.warning('Adding new anime: %s %s', str(item["mal_id"]), anime["_id"])
            dup_title = self.db.anime.find_one({"title": anime["title"]})
            if dup_title:
                anime["dup_title"] = True
                self.log.error('New anime (%s) title (%s) was a duplicate: %s was set as ID.', str(item["mal_id"]), item["title"], anime["_id"])
        else:
            anime["_id"] = prev["_id"]
            del anime["title"]
        
        upd = self.db[self.coll].update_one({"external_links.mal":item["mal_id"]}, {"$set":anime}, upsert=True)
        return item

    def resolve_relations(self, rel):
        relations = {}
        if rel.get("Adaptation"):
            relations["Adaptation"] = []
            for mrel in rel["Adaptation"]:
                manga = self.db.manga.find_one({"external_links.mal":mrel[1]}, {"title":1, "type":1})
                if not manga:
                    manga = self.db.novel.find_one({"external_links.mal":mrel[1]}, {"title":1, "type":1})
                if not manga:
                    self.log.critical("Missing manga (Adaptation): %s", str(mrel[1]))
                    relations["Adaptation"].append({"missing_ref":"manga", "missing_id":mrel[1]})
                    continue
                ref = "novel" if manga["type"] == "Novel" else "manga"
                relations["Adaptation"].append(DBRef(ref, manga["_id"], title = manga["title"]))
            del rel["Adaptation"]

        for (t, rels) in rel.items():
            relations[t] = []
            for arel in rel[t]:
                anime = self.db.anime.find_one({"external_links.mal":arel[1]}, {"title":1})
                if not anime:
                    self.log.critical("Missing anime ("+t+"): %s", str(arel[1]))
                    relations[t].append({"missing_ref":"anime", "missing_id":arel[1]})
                    continue
                relations[t].append(DBRef("anime", anime["_id"], title = anime["title"]))
        return relations

    def remove_number(self, str):
        return re.sub("""^#\d+:? ?""", "", str)

    def duration(self, duration_string):
        values = {"hr":60*60, "min":60, "sec":1}
        time_strs = [t for t in duration_string.split(" ") if (t != "per" and t != "ep.")]
        duration_sec = 0
        for (amount, unit) in zip(time_strs[::2], time_strs[1::2]):
            duration_sec += int(amount) * values[unit[:-1]]
        return duration_sec

    def parse_synonyms(self, synonyms):
        syns = {}
        for syn in synonyms:
            if syn[0:8] == "English:":
                syns["english"] = syn[8:]
            elif syn[0:9] == "Japanese:":
                syns["japanese"] = syn[9:]
            elif syn[0:9] == "Synonyms:":
                syns["synonyms"] = syn[9:]
        return syns
