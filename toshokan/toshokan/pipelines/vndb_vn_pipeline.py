from datetime import datetime
import re
import json
import scrapy
from pymongo import InsertOne, UpdateOne
from bson.dbref import DBRef

from .mal_pipeline import LibraryPipeline

class VNDBVNPipeline(LibraryPipeline):
    coll = "vn"
    def open_spider(self, *args, **kwargs):
        super().open_spider(*args, **kwargs)
        self.load_tags()
        #self.add_tags()
        
    def load_tags(self):
        with open("/var/lib/scrapyd/files/tags.json", "r", encoding="utf-8") as f:
            self.tagsdb = json.load(f)
        self.tagsdict = {}
        for tag in self.tagsdb:
            self.tagsdict[tag["id"]] = tag

    def add_tags(self):
        ops = []
        for tag in self.tagsdb:
            ops.append(UpdateOne({
                "vndb": tag["id"]
            },{"$set": {
                "current_name": tag["name"],
                "aliases": tag["aliases"],
                "description": tag["description"],
                "meta": tag["meta"],
                "vn_count": tag["vns"],
                "type": tag["cat"],
                "parents": [self.tagsdict[t]["name"] for t in tag["parents"]],
                "last_modified": datetime.utcnow()
            },
            "$setOnInsert": {
                    "_id": self.create_id("vn_tag", None, tag["name"]),
                    "vndb": tag["id"],
                    "name": tag["name"],
                    "date_added": datetime.utcnow()
                }
            }, upsert=True))
        self.db.vn_tag.bulk_write(ops)

    def process_item(self, item, spider):
        if spider.name != "VNDB_API_VN":
            return item
        tags = self.process_tags(item["tags"])

        if item["released"] == 'tba':
            period = None
            start_date = None
            tba = True
        else:
            tba = False
            period, start_date = self.parse_date(item["released"])

        vn = {
            "title": item["title"],
            "image": item["image"],
            "image_nsfw": item["image_nsfw"],
            "description": item["description"],
            "languages": item["languages"],
            "orig_lang": item["orig_lang"],
            "synonyms": {"synonyms": item["aliases"], "japanese": item["original"]},
            "length": item["length"],
            "start_date": start_date,
            "tba": tba,
            "start_date_period": period,
            "n_scores": item["votecount"],
            "score": item["rating"],
            "popularity_rank": item["popularity"],
            "platforms": item["platforms"],
            "external_links": {"vndb": item["id"], **item["links"]},
            "tags": tags["list"],
            "tag_data": tags["data"],
            "screens": item["screens"],
            "last_modified": datetime.utcnow(),
        }
        prev = self.db[self.coll].find_one({"external_links.vndb": item["id"]})
        if not prev:
            vn["_id"] = self.create_id(self.coll, item["id"], item["title"])
            vn["new"] = True
            self.log.warning('Adding new VN: %s %s', str(item["id"]), vn["_id"])
            dup_title = self.db[self.coll].find_one({"title": item["title"]})
            if dup_title:
                vn["dup_title"] = True
                self.log.error('New VN (%s) title (%s) was a duplicate: %s was set as ID.', str(item["id"]), item["title"], vn["_id"])
        else:
            vn["_id"] = prev["_id"]
            del vn["title"]
        self.process_anime(item["anime"], vn["_id"], item["id"], item["title"])
        self.process_relations(item["relations"], vn["_id"], item["id"], item["title"])
        upd = self.db[self.coll].update_one({"external_links.vndb":item["id"]}, {"$set":vn, "$setOnInsert": {"date_added": datetime.utcnow()}}, upsert=True)
        return item

    def parse_date(self, date_string):
        if not date_string:
            return None, None
        try:
            return "day", datetime.strptime(date_string, '%Y-%m-%d')
        except:
            pass
        try:
            return "month", datetime.strptime(date_string, '%Y-%m')
        except:
            pass
        return "year", datetime.strptime(date_string, '%Y')

    def process_tags(self, tag_array):
        tags = {"list": [], "data": {}}
        for tag in tag_array:
            try:
                name = self.tagsdict[tag[0]]["name"]
                tags["list"].append(name)
                tags["data"][name] = { "score": tag[1], "spoiler_level": tag[2] }
            except:
                continue
        return tags

    def process_anime(self, anime_list, _id, vndb_id, vn_title):
        ops = []
        for anime in anime_list:
            ops.append(UpdateOne(
            {
                "vn": _id,
                "anidb": anime["id"],
            },
            {"$set": {
                "vn": _id,
                "vndb": vndb_id,
                "anime": None,
                "anidb": anime["id"],
                "ann": anime["ann_id"],
                "nfo": anime["nfo_id"],
                "type": anime["type"],
                "vn_title": vn_title,
                "anime_title": anime["title_romaji"],
                "anime_title_kanji": anime["title_kanji"],
                "year": anime["year"],
                "vn_to_anime": True,
                "last_modified": datetime.utcnow(),
            }, "$setOnInsert": {"date_added": datetime.utcnow()}}, upsert=True))
        if ops:
            self.db.vn_anime_relations.bulk_write(ops)

    def process_relations(self, relations, _id, vndb_id, title):
        ops = []
        for rel in relations:
            other = self.db[self.coll].find_one({"external_links.vndb": rel["id"]})
            ops.append(UpdateOne(
                {"from":_id}, 
                {"$set":{
                    "from": _id,
                    "to": other["_id"] if other else None,
                    "vndb": {"from": vndb_id, "to": rel["id"]},
                    "relation": rel["relation"],
                    "from_title": title,
                    "to_title": rel["title"],
                    "official": rel["official"]
                }, "$setOnInsert": {"date_added": datetime.utcnow()}}, upsert=True))
            self.db.vn_relations.update_one({"vndb.to": vndb_id, "to": None}, {"$set": {"to": _id}})
        if ops:
            self.db.vn_relations.bulk_write(ops)