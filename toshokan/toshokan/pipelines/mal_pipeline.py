from .mongo_pipeline import MongoPipeline
import dateparser

from .libid import library_id

class LibraryPipeline(MongoPipeline):
    def create_id(self, typedb, mal_id, title, synonyms=None):
        naive_id = self.library_id(title, synonyms=synonyms)
        number = 0
        while True:
            if not number:
                attempt = naive_id
            else:
                attempt = naive_id+"-"+str(number)

            same_id = self.db[typedb].find_one({"_id":attempt})
            if same_id:
                number += 1
                continue
            else:
                break
        return attempt

    def library_id(self, title, synonyms=None):
        if title == "!":
            return "!"
        if title == "≠":
            return "not-equal"
        if title == "?":
            return library_id("甲斐の虎 武田信玄")
        return library_id(title)

class MALPipeline(LibraryPipeline):
    def air_time(self, time_str):
        no_date = {'date_obj': None, 'period': 'day', 'locale': None}
        if time_str == "Not available":
            return (no_date, no_date)

        dates = [dateparser.date.DateDataParser().get_date_data(d) for d in time_str.split(" to ")]
        if len(dates) == 1:
            return (dates[0], no_date)
        if len(dates) == 0:
            return (no_date, no_date)
        return dates