from datetime import datetime
import re

import scrapy
from pymongo import InsertOne, UpdateOne
from bson.dbref import DBRef

from .mal_pipeline import MALPipeline

class MALAnimeStaffPipeline(MALPipeline):
    coll = "anime_%s"

    def process_item(self, item, spider):
        if spider.name != "MAL_anime_staff":
            return item

        if item == None:
            raise scrapy.exceptions.DropItem

        anime = self.db["anime"].find_one({"external_links.mal":item["anime"]}, {"title":1})
        if not anime:
            print("Anime %d not found!" % item["anime"])
            raise ValueError
        anime_ref = DBRef("anime", anime["_id"], title=anime["title"])
        staff_ops = [UpdateOne(
                                {"anime.$id": anime_ref.id, "person.$id": self.get_staff(staff).id},
                                {"$set":{
                                "anime":anime_ref,
                                "person": self.get_staff(staff),
                                "roles": staff["description"].split(", "),
                                "last_modified": datetime.utcnow()
                               }, "$setOnInsert": {"date_added": datetime.utcnow()}}, upsert=True) for staff in item["staff"]]
        character_ops = [UpdateOne(
                                    {"anime.$id": anime_ref.id, "character.$id": self.parse_cast(cast).id},
                                    {"$set":{
                                    "anime":anime_ref,
                                    "character": self.parse_cast(cast),
                                    "type": cast["role"],
                                    "voices": [self.get_va(va) for va in cast["people"]],
                                    "last_modified": datetime.utcnow()
                                    }, "$setOnInsert": {"date_added": datetime.utcnow()}}, upsert=True) for cast in item["voiceroles"]]
        if staff_ops:
            self.db[self.coll%"staff"].bulk_write(staff_ops)
        if character_ops:
            self.db[self.coll%"cast"].bulk_write(character_ops)

    def parse_cast(self, cast):
        char = self.db["character"].find_one({"external_links.mal":cast["character"]}, {"_id":1, "nick_name":1})
        if not char:
            print("Character %d not found!" % cast["character"])
            raise ValueError
        return DBRef("character", char["_id"], name=char["nick_name"])
    def get_va(self, va):
        person = self.db["person"].find_one({"external_links.mal":va["person"]}, {"_id":1, "name":1})
        if not person:
            print("Person %d not found! (va)" % va["person"])
            raise ValueError
        return DBRef("person", person["_id"], name=person["name"], language=va["role"])

    def get_staff(self, staff):
        person = self.db["person"].find_one({"external_links.mal":staff["person"]}, {"_id":1, "name":1})
        if not person:
            print("Person %d not found! (staff)" % staff["person"])
            raise ValueError
        return DBRef("person", person["_id"], name=person["name"])