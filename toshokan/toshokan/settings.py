# -*- coding: utf-8 -*-
import time
import os

# Scrapy settings for toshokan project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html

MAL_MANGA_PATH = "file://///var/lib/scrapyd/files/manga_mal/%s.html"
MAL_ANIME_PATH = "file://///var/lib/scrapyd/files/anime_mal/%s.html"
MAL_ANIME_STAFF_PATH = "file://///var/lib/scrapyd/files/staff_anime_mal/%s.html"
MAL_CHARACTER_PATH = "file://///var/lib/scrapyd/files/character_mal/%s.html"
MAL_PERSON_PATH = "file://///var/lib/scrapyd/files/person_mal/%s.html"
MAL_MANGA_STAFF_PATH = "file://///var/lib/scrapyd/files/staff_manga_mal/%s.html"

BOT_NAME = 'toshokan'

SPIDER_MODULES = ['toshokan.spiders']
NEWSPIDER_MODULE = 'toshokan.spiders'
LOG_STDOUT = False
#LOG_FILE = "/var/lib/scrapyd/logs/"+str(int(time.time()))+'-log.txt'

MONGO_URI = os.getenv('MONGODB_URI', "")
if not MONGO_URI:
    pass
    #with open('dbcredentials.txt') as f:
    #    MONGO_URI = f.readline()
MONGO_DATABASE = "toshokan"
VNDB_USER = os.getenv('VNDB_USER', "")
VNDB_PASS = os.getenv('VNDB_PASS', "")
ITEM_PIPELINES = {
    'toshokan.pipelines.MALAnimePipeline': 100,
    'toshokan.pipelines.MALMangaPipeline': 200,
    'toshokan.pipelines.MALCharacterPipeline': 300,
    'toshokan.pipelines.MALPersonPipeline': 400,
    'toshokan.pipelines.MALAnimeStaffPipeline': 500,
    'toshokan.pipelines.MALMangaStaffPipeline': 600,
    'toshokan.pipelines.MALEpisodePipeline': 600
}
# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'toshokan (+http://www.yourdomain.com)'

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://doc.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
#DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable spider middlewares
# See https://doc.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'toshokan.middlewares.ToshokanSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
#    'toshokan.middlewares.ToshokanDownloaderMiddleware': 543,
}

RETRY_HTTP_CODES = [500, 503, 504, 400, 408, 429]
ROBOTSTXT_OBEY = False
# Enable or disable extensions
# See https://doc.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
#ITEM_PIPELINES = {
#    'toshokan.pipelines.ToshokanPipeline': 300,
#}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/autothrottle.html
AUTOTHROTTLE_ENABLED = False
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 1.0
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 3
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

