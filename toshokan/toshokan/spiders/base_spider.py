import scrapy
import logging

import pymongo

class BaseSpider(scrapy.Spider):
    """Base class for Spiders"""
    def __init__(self,  *args, s=1, e=1, **kwargs):
        super().__init__(*args, **kwargs)
        self.start = int(s)
        self.end = int(e)
        # Setup logging
        self.log = logging.getLogger('scrapy')
        self.log.setLevel(logging.WARNING)
    def start_requests(self):
        # Setup mongodb
        self.client = pymongo.MongoClient(self.settings.get('MONGO_URI'))
        self.db = self.client[self.settings.get('MONGO_DATABASE')]