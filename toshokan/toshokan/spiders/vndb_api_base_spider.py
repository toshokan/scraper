import scrapy

from .base_spider import BaseSpider

class VNDBAPIRequest(scrapy.Request):
    def __init__(self, callback, _id = 1, dtype = "vn"):
        super().__init__(url="http://example.com", callback=callback, dont_filter=True)
        self._id = _id
        self.dtype = dtype

class VNDBAPI_Base_Spider(BaseSpider):
    """Base class for VNDB API Spiders"""
    custom_settings = {
        'DOWNLOADER_MIDDLEWARES' : {
            'toshokan.middlewares.VNDBAPIDownloader': 1,
            # Disable all downloader middleware
            'scrapy.downloadermiddlewares.robotstxt.RobotsTxtMiddleware': None,
            'scrapy.downloadermiddlewares.httpauth.HttpAuthMiddleware': None,
            'scrapy.downloadermiddlewares.downloadtimeout.DownloadTimeoutMiddleware': None,
            'scrapy.downloadermiddlewares.defaultheaders.DefaultHeadersMiddleware': None,
            'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
            'scrapy.downloadermiddlewares.retry.RetryMiddleware': None,
            'scrapy.downloadermiddlewares.ajaxcrawl.AjaxCrawlMiddleware': None,
            'scrapy.downloadermiddlewares.redirect.MetaRefreshMiddleware': None,
            'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': None,
            'scrapy.downloadermiddlewares.redirect.RedirectMiddleware': None,
            'scrapy.downloadermiddlewares.cookies.CookiesMiddleware': None,
            'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': None,
            'scrapy.downloadermiddlewares.stats.DownloaderStats': None,
            'scrapy.downloadermiddlewares.httpcache.HttpCacheMiddleware': None,
        },
        "ITEM_PIPELINES" : {
        'toshokan.pipelines.VNDBVNPipeline': 100,
        'toshokan.pipelines.VNDBCharacterPipeline': 200,
        }
    }
    dtype = "vn"
    get_more = True
    def start_requests(self):
        yield VNDBAPIRequest(callback=self.parse, _id = self.start, dtype=dtype)

    def parse(self, response):
        res = response.content
        items = list(res["items"])

        if res.get("more") and get_more:
            _id = res["items"][-1]["id"]
            if not (self.end > 1 and _id >= self.end):
                yield VNDBAPIRequest(callback=self.parse, _id = _id+1, dtype=dtype)

        for item in items:
            yield item

class VNDBAPIVNSpider(VNDBAPI_Base_Spider):
    """Scraper for VNs from VNDB's API."""
    name = "VNDB_API_VN"
    dtype = "vn"

class VNDBAPICharacterSpider(VNDBAPI_Base_Spider):
    """Scraper for Characters from VNDB's API."""
    name = "VNDB_API_Character"
    dtype = "character"

class VNDBAPIStaffSpider(VNDBAPI_Base_Spider):
    """Scraper for Staff from VNDB's API."""
    name = "VNDB_API_Staff"
    dtype = "staff"
    get_more = False

class VNDBAPIReleaseSpider(VNDBAPI_Base_Spider):
    """Scraper for Releases from VNDB's API."""
    name = "VNDB_API_Release"
    dtype = "release"

class VNDBAPIProducerSpider(VNDBAPI_Base_Spider):
    """Scraper for Producers from VNDB's API."""
    name = "VNDB_API_Producer"
    dtype = "producer"