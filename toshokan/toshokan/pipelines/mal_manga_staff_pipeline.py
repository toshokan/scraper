from datetime import datetime
import re

import scrapy
from pymongo import InsertOne, UpdateOne
from bson.dbref import DBRef

from .mal_pipeline import MALPipeline

class MALMangaStaffPipeline(MALPipeline):
    coll = "%s_cast"

    def process_item(self, item, spider):
        if spider.name != "MAL_manga_staff":
            return item

        if item == None:
            raise scrapy.exceptions.DropItem
        manga = self.db["manga"].find_one({"external_links.mal":item["manga"]}, {"_id":1, "title":1})
        typedb = "manga"
        if not manga:
            manga = self.db["novel"].find_one({"external_links.mal":item["manga"]}, {"_id":1, "title":1})
            typedb = "novel"

        cast = [{
                typedb: DBRef(typedb, manga["_id"], title=manga["title"]),
                "character": self.get_character(char),
                "type": char["description"],
                'last_modified': datetime.utcnow()
            } for char in item["characters"]]

        cast_ops = [UpdateOne({typedb+".$id": char[typedb].id, "character.$id": char["character"].id}, {"$set":char, "$setOnInsert": {"date_added": datetime.utcnow()}}, upsert=False) for char in cast]
        if cast:
            self.db[self.coll % typedb].bulk_write(cast_ops)
        return item

    def get_character(self, char):
        character = self.db["character"].find_one({"external_links.mal":char["character"]}, {"_id":1, "nick_name":1})
        if not character:
            print("Character %d not found!" % char["character"])
            raise ValueError
        return DBRef("character", character["_id"], name=character["nick_name"])