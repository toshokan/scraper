import scrapy
import json
import logging
from .base_spider import BaseSpider
class MALAnimeStaffSpider(BaseSpider):
    """Scraper for staff roles and characters for anime on MyAnimeList."""
    name = "MAL_anime_staff"

    def start_requests(self):
        super().start_requests()
        anime_list = [int(a["external_links"]["mal"]) for a in list(self.db.anime.find({}, {"external_links.mal":1}))]
        anime_list.sort()
        anime_list = [i for i in anime_list if i >= self.start and i <= self.end]
        for anime_id in anime_list:
            anime_id = int(anime_id)
            yield scrapy.Request(url=self.settings["MAL_ANIME_STAFF_PATH"] % str(anime_id).format(anime_id),
                                 callback=self.parse)

    def parse(self, response):
        voiceroles = []
        staff = []

        anime_id = int(response.url.split('/')[-1].split(".")[-2])
        print(str(anime_id))
        i: int = 1
        while True:
            table = response.xpath("//*[@id='content']/table/tr/td[2]/div[1]/table[{}]/tr".format(i)).extract_first()
            i += 1

            if not table:
                break

            if "myanimelist.net/character" in table:
                # We're dealing with a character/VA duo
                character_id = int(table.split('myanimelist.net/character/')[1].split('/')[0])
                character_role = table.split('<small>')[1].split('</small>')[0]

                people = []
                s = table.split('myanimelist.net/people/')
                for j in range(1, len(s), 2):
                    person = int(s[j].split('/')[0])
                    role = s[j].split('<small>')[1].split('</small>')[0]
                    people.append({'person':person, 'role':role})

                voiceroles.append({'character':character_id, 'role':character_role, 'people':people})

            else:
                # Just a staff role
                person = int(table.split('myanimelist.net/people/')[1].split('/')[0])
                description = table.split('<small>')[1].split('</small>')[0]
                staff.append({'person':person, 'description':description})
        yield {'anime':anime_id, 'voiceroles':voiceroles, 'staff':staff}
