from datetime import datetime
import re

import scrapy

from .mal_pipeline import MALPipeline

class MALPersonPipeline(MALPipeline):
    coll = 'person'

    def process_item(self, item, spider):
        if spider.name != "MAL_person":
            return item

        if item == None:
            raise scrapy.exceptions.DropItem

        person = {
            'name': item["name"],
            'image': item["main_img"],
            'given_name': item["given_name"],
            'family_name': item["family_name"],
            'birthday': item["birthday"],
            'website':item["website"],
            'more': item["more"],
            "external_links":{"mal":item["mal_id"]},
             'last_modified': datetime.utcnow()
            }
        prev = self.db.person.find_one({"external_links.mal":item["mal_id"]})
        if not prev:
            person["_id"] = self.create_id(self.coll, item["mal_id"], item["name"])
            person["date_added"] = datetime.utcnow()
            self.log.warning('Adding new person: %s %s', str(item["mal_id"]), person["_id"])

            if person["_id"] != self.library_id(item["name"]):
                person["dup_id"] = True
                self.log.error('New person (%s) computed ID %s was a duplicate: %s was used instead.', str(item["mal_id"]), self.library_id(item["name"]), person["_id"])
        else:
            person["_id"] = prev["_id"]
        self.db[self.coll].update_one({"external_links.mal":item["mal_id"]}, {"$set": person}, upsert=True)
        return item