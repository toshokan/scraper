# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html

#class ToshokanPipeline(object):
#    def process_item(self, item, spider):
#        return item

from .mal_anime_pipeline import MALAnimePipeline
from .mal_manga_pipeline import MALMangaPipeline
from .mal_character_pipeline import MALCharacterPipeline
from .mal_person_pipeline import MALPersonPipeline
from .mal_anime_staff_pipeline import MALAnimeStaffPipeline
from .mal_manga_staff_pipeline import MALMangaStaffPipeline
from .mal_episode_pipeline import MALEpisodePipeline
from .vndb_vn_pipeline import VNDBVNPipeline
from .vndb_character_pipeline import VNDBCharacterPipeline