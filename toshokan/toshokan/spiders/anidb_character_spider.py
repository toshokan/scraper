import scrapy, re, logging

class AniDBCharacterSpider(scrapy.Spider):
    name = "anidb_character"

    def start_requests(self):
        yield scrapy.Request(url='file://127.0.0.1/home/kahr/character.html', callback=self.pasre)


    def parse(self, response):
        out = {}
        # TODO uid
        uid = 23271

        out['main_name'] = response.xpath("//tr[contains(., //th[contains(./text(), 'Main Name')])]/td[@class='value']/span[@itemprop='name']/text()").extract_first()

        out['alt_names'] = []
        alt_names = response.xpath("//div[@id='tab_2_pane']//tr[@class='official verified yes' and contains(., //th[contains(./text(), 'Official Name')])]//label[@itemprop='alternateName']/text()").extract()
        alt_languages =[x.replace('language: ', '') for x in
                        response.xpath("//div[@id='tab_2_pane']//span[contains(@class, 'i_icon') and contains(@title, 'language:')]/*title").extract()]
        for i in range(len(alt_names)):
            out['alt_names'].append({'name': alt_names[i], 'language':alt_languages[i]})

        out['birth_date'] = response.xpath("//span[@itemprop='birthDate']/text()").extract_first()
        out['death_date'] = response.xpath("//span[@itemprop='deathDate']/text()").extract_first()
        out['age'] = response.xpath("//tr[contains(@class, 'age')]/td[@class='value']/text()").extract_first()
        out['gender'] = response.xpath("//span[@itemprop='gender']/text()").extract_first()
        out['tags'] = response.xpath("//div[@id='tab_1_pane']//span[@class='tagname']/text()").extract()
        out['description'] = response.xpath("//div[contains(@class, 'desc')]/text()").extract_first()

        sout = {}
        staff = []
        # We need the current character uid to be set for this!
        for row in response.xpath("//table[@id=seiyuulist_{}]//tr".format(uid)).extract()[1:]:
            if 'creatorid' in row:
                if sout:
                    staff.append(sout)

                sout = {'actor': int(row.split('creatorid=')[1].split('">')[0]),
                        'roles':[]}

            try:
                aid = int(row.split('aid=')[1].split('">')[0])
            except:
                aid = None

            relation = row.split('type ">')[1].split('\n')[0]
            main = row.split('ismainseiyuu ">')[1].split('\n')[0]
            comment = row.split('comment ">')[1].split('</td>')[0]

            sout['roles'].append({
                'relation':relation,
                'main':main,
                'comment':comment,
                'aid':aid
            })


        out['staff'] = staff

        out['anime_appearance']
        for row in response.xpath("//table[@id=animelist_{}]//tr".format(uid)).extract()[1:]:
            aid = int(row.split('charanimerelid_')[1].split('"')[0])
            role = row.split('type relation">')[1].split('</td>')[0]
            episodes = row.split('eprange">')[1].split('</td>')[0]

            out['anime_appearance'].append({
                'aid':aid,
                'role':role,
                'episodes':episodes
            })

        out['related_entities'] = []
        cout = {}
        for row in response.xpath("//table[@id=characterlist_{}]//tr".format(uid)).extract()[1:]:
            if 'reltype' in row:
                if cout:
                    out['related_entities'].append(cout)

                cout = {'type': row.split('reltype">')[1].split('</td>')[0],
                        'characters': []}

            cout['characters'].append(int(row.split('charrelid')[1].split('"')[0]))

        yield out
            
