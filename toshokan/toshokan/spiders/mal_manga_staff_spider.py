import scrapy
import json

from .base_spider import BaseSpider

class MALMangaStaffSpider(BaseSpider):
    """Scraper for staff roles and characters for anime on MyAnimeList."""
    name = "MAL_manga_staff"
    def start_requests(self):
        super().start_requests()
        manga = [int(a["external_links"]["mal"]) for a in list(self.db.manga.find({}, {"external_links.mal":1}))]
        novels = [int(a["external_links"]["mal"]) for a in list(self.db.novel.find({}, {"external_links.mal":1}))]
        manga_novels = sorted(manga+novels)
        manga_novels = [i for i in manga_novels if i >= self.start and i <= self.end]
        for manga_id in manga_novels:
            yield scrapy.Request(url=self.settings["MAL_MANGA_STAFF_PATH"] % int(manga_id),
                                 callback=self.parse)

    def parse(self, response):
        characters = []

        manga_id = int(response.url.split('/')[-1].split(".")[-2])

        i: int = 1
        while True:
            table = response.xpath("//*[@id='content']/table/tr/td[2]/div[1]/table[{}]/tr".format(i)).extract_first()
            i += 1

            if not table:
                break

            character = int(table.split('myanimelist.net/character/')[1].split('/')[0])
            description = table.split('<small>')[1].split('</small>')[0]
            characters.append({'character':character, 'description':description})

        yield {'manga':manga_id, 'characters':characters}
