import re
from slugify import slugify, SLUG_OK
from pykakasi import kakasi
kakasi_inst = kakasi()
kakasi_inst.setMode("H","a") # Hiragana to ascii, default: no conversion
kakasi_inst.setMode("K","a") # Katakana to ascii, default: no conversion
kakasi_inst.setMode("J", "a") # Japanese to ascii, default: no conversion
kakasi_inst.setMode("s", True)
kconv = kakasi_inst.getConverter()

def library_id(title, synonyms=None):
    title_clean = re.sub("""[☆★＊]+""", '*', title)
    title_clean = re.sub("""[×]+""", 'x', title_clean)
    title_clean = re.sub("""[␣]+""", '_', title_clean)
    title_clean = re.sub("""[†]+""", '+', title_clean)
    title_clean = re.sub("""[十]+""", '+', title_clean)
    title_clean = re.sub("""[卍]+""", ' ', title_clean)
    title_clean = re.sub("""[♭]+""", 'b', title_clean)
    title_clean = re.sub("""[%]+""", ' percent ', title_clean)
    title_clean = re.sub("""[&]+""", ' and ', title_clean)
    title_clean = title_clean[0]+re.sub("""[.]+""", ' ', title_clean[1:]) #only replace after first character
    title_clean = re.sub("""[—∀®^​→:'”=“\]ー\[(/◎≠,♡⤴￥\"↑♪;#∞!°>♂–’▲△?)♥・]+""", ' ', title_clean)
    title_clean = re.sub('[½]+', '1-2', title_clean)

    if re.search("""[\u4E00-\u9FAF\u3040-\u3096\u30A1-\u30FA\uFF66-\uFF9D\u31F0-\u31FF]""", title_clean):
        title_clean = kconv.do(title_clean) # translate into romaji

    title_clean = slugify(title_clean, only_ascii=True, ok=SLUG_OK+"@$_+*.○◯")
    library_id = title_clean = re.sub("""[○◯]+""", 'O', title_clean)

    if not library_id:
        raise ValueError

    return library_id